﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public User Give { get; set; }
        public User Take { get; set; }
        public int Sum { get; set; }
        public DateTime date { get; set; }
    }
}
