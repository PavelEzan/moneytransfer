﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer
{
    public class Generator
    {
        public int GetGenerator()
        {

            Random rnd = new Random();
            int code = Convert.ToInt32(string.Format("{0:d6}", rnd.Next(0, 999999)));

            return code;
        }

        public bool Exist(List<int> code, int c)
        {
            if (code.Contains(c)) return false;
            return true;
        }
    }
}
